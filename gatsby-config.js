module.exports = {
  siteMetadata: {
    title: 'Design+Code React for Designers',
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
